# Simple Django Blogs App #
## What is this? ##
Another test task for hiring :)
## What it does? ##
Users have a personal blog. They can't create it.
But they can subscribe to each other blog posts and receive email notifications when a new post will be available.
## How to run? ##
1. Create a clean virtual environment with python3 interpreter.
2. Go to main application's directory and run pip install -r requirements.txt
3. Copy template_local_settings_and_secrets.json to local_settings_and_secrets.json and fill in ALL settings credentials.
4. Generate a new secret key for Django project - http://www.miniwebtool.com/django-secret-key-generator/ and paste it into local_settings_and_secrets.json file
5. Now we need create new database migrations. Run: python manage.py makemigrations <appname1> <appname2> ans so on.
6. Then apply newly created migrations: python manage.py migrate
7. Create a Django admin by running command: python manage.py createsuperuser
8. Launch application by running command "python manage.py runserver 0.0.0.0:8000"
9. Open http://your_server_address:8000 in your browser. All should work.

## List of project apps ##
* blogs

## Prerequisites ##
* Python 3

## Database Tips ##

## PIP & Virtualenv Tips ##
* If you have no PIP installed on your system (check with 'pip freeze' command) then run: sudo apt install python-pip
* Now update pip: pip install --upgrade pip
* Then you need to install virtualenv: sudo pip install virtualenv
* Also you need install virtualenvwrapper: sudo pip install virtualenvwrapper
* All packages should be installed into activated virtual environment without sudo: eg. pip install requests

### Configuring Virtualenvwrapper with Shell Startup File (.bashrc) ###
* Create a directory where will be stored all your virtualenvs: mkdir ~/.virtualenvs
* Then configure environment variables as described at http://virtualenvwrapper.readthedocs.io/en/latest/install.html#shell-startup-file
* (Optional) If something goes wrong - you may install virtualenvwrapper when you at virtualenv: pip install virtualenvwrapper
