from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.admin import User
from django.template.loader import render_to_string

from django_blogs_project.settings import DEFAULT_FROM_EMAIL, SITE_DOMAIN


class SubscriptionsTable(models.Model):
    subscriber = models.ForeignKey(User, related_name='subscriber')
    subscribed_to = models.ForeignKey(User, related_name='subscribed_to')


class UserProfile(models.Model):
    """
    Extends builtin User model
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def get_user_subscriptions(self):
        print(SubscriptionsTable.objects.filter(subscriber=self.user))

    def get_user_subscribers(self):
        return SubscriptionsTable.objects.filter(subscribed_to=self.user)

    def is_current_user_already_subscribed(self, blog_id):
        """
        Check self.user with blog_id User instance to find out is self.user already subscribed on blog_id User.
        :param blog_id: It is a some User object which needs to be checked with current authorized self.user object.
        :return:
        """
        for subscriber in self.user.subscriber.all():
            # If we have blog_id/User in
            if subscriber.subscribed_to == blog_id:
                return True
        return False

    class Meta:
        verbose_name = 'user profile'
        verbose_name_plural = 'user profiles'

    def __unicode__(self):
        return self.user.username

    def str(self):
        return self.user.username


class Post(models.Model):
    title = models.CharField(max_length=128, blank=False)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    blog = models.ForeignKey(User)  # Same as user/User

    def create_rss_post_entry_and_notify_users(self, blog_subscribers, current_host):
        for user_subscriber in blog_subscribers:
            rss_post_object = RSSPost.objects.get_or_create(
                user=user_subscriber.subscriber,
                post=self,
                read=False
            )[0]
            rss_post_object.notify_user_about_new_post(current_host)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title


class RSSPost(models.Model):
    user = models.ForeignKey(User)
    post = models.ForeignKey(Post)
    # Means a user has read a post or not. Default = he wasn't read.
    read = models.BooleanField(default=False)

    def notify_user_about_new_post(self, website_domain):
        """
        Send out email to subscribed user about new published post
        :return:
        """
        payload = {
            "subscriber_name": self.user.first_name,
            "subscription_blog_name": self.post.blog,
            "subscription_post_name": self.post,
            "subscription_blog_id": self.post.blog.id,
            "subscription_post_id": self.post.id,
            "website_domain": website_domain,
        }
        html_message_to_send = render_to_string('email/new_post_notification_email.html', context=payload)
        txt_message_to_send = render_to_string('email/new_post_notification_email.txt', context=payload)
        try:
            send_mail(
                subject='A New Post Was Published in {}'.format(website_domain),
                from_email=DEFAULT_FROM_EMAIL,
                recipient_list=[self.user.email],
                message=txt_message_to_send,
                html_message=html_message_to_send,
                fail_silently=False
            )
        except Exception as err:
            # rollbar.report_message(err.message)
            print(err)


# When the new user created we need to create or update UserProfile.
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


# Updating UserProfile when a User was updated/saved
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()


# Refresh News Feed after new Post created
@receiver(post_save, sender=Post)
def create_rss_record_notify_users(sender, instance, **kwargs):
    blog_subscribers = instance.blog.userprofile.get_user_subscribers()
    instance.create_rss_post_entry_and_notify_users(blog_subscribers, SITE_DOMAIN)
