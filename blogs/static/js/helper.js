function mark_post_as_read (param) {
    $.ajax({
        url : "/feed/mark-read/" + param,
        type : "GET",
        success : function(json) {
            console.log(json);
            $("#label_new_post_" + param).empty().append(
                '<span class="label label-default" id="button">Read</span>')
        },
        error : function(xhr,errmsg,err) {
            alert(xhr.status + ": " + xhr.responseText);
        }
    });
}
