from django.conf.urls import url
from . import views

# blog_id == user.id
urlpatterns = [
    url(r'^$', views.BlogsList.as_view(), name='blogs_list'),
    url(r'^post/add/$', views.PostAdd.as_view(), name='post_add'),
    url(r'^post/edit/(?P<pk>[0-9]+)/$', views.PostUpdate.as_view(), name='post_edit'),
    url(r'^blog/(?P<blog_id>[0-9]+)/$', views.PostsList.as_view(), name='blog_posts'),
    url(r'^blog/(?P<blog_id>[0-9]+)/subscribe/$', views.BlogSubscribe.as_view(), name='blog_subscribe'),
    url(r'^blog/(?P<blog_id>[0-9]+)/unsubscribe/$', views.BlogUnsubscribe.as_view(), name='blog_unsubscribe'),
    url(r'^blog/(?P<blog_id>[0-9]+)/(?P<pk>[0-9]+)/$', views.PostDetail.as_view(), name='post_detail'),
    url(r'^feed/$', views.NewsFeed.as_view(), name='news_feed'),
    url(r'^feed/mark-read/(?P<rss_post_id>[0-9]+)/', views.MarkPostAsRead.as_view(), name='feed_mark_read'),
]
