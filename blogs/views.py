from django.contrib import messages
from django.shortcuts import get_object_or_404, reverse, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, View
from django.views.generic.base import ContextMixin
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseForbidden, JsonResponse

from django_blogs_project.settings import SITE_DOMAIN
from .models import Post, SubscriptionsTable, RSSPost


class ShowSubscribersToUser(ContextMixin, View):
    def get_context_data(self, **kwargs):
        # user_subscriptions = req
        # context = super(ShowSubscribersToUser, self).get_context_data(**kwargs)
        # context["user_subscriptions"] = user_object
        # return context
        pass


# List all available Blogs
class BlogsList(ListView):
    model = User
    template_name = 'blogs_app/blogs_list.html'

    def get_queryset(self):
        # Not showing if admin and own blog.
        return User.objects.exclude(is_staff=True).exclude(id=self.request.user.id)


# List Posts from given as pk in url Blog
class PostsList(ListView):
    """
    Possible, we could add handler which doesn't allow show staff's blogs. But not in this scope :)
    """
    model = Post
    context_object_name = 'posts_list'
    paginate_by = 5
    template_name = 'blogs_app/posts_list.html'

    def get_queryset(self):
        return Post.objects.filter(blog=self.kwargs.get("blog_id")).order_by('-created')

    def get_context_data(self, **kwargs):
        blog_owner_user_object = get_object_or_404(User, pk=self.kwargs.get("blog_id"))
        context = super(PostsList, self).get_context_data(**kwargs)
        context["blog_owner"] = blog_owner_user_object
        try:
            context["already_subscribed"] = self.request.user.userprofile.is_current_user_already_subscribed(blog_owner_user_object)
        except AttributeError:
            # If user not authenticated then he is a 'AnonymousUser'
            context["already_subscribed"] = False
        return context


# Detail Post View
class PostDetail(DetailView):
    model = Post
    template_name = 'blogs_app/post_detail.html'

    def get_queryset(self):
        return Post.objects.filter(blog=self.kwargs.get("blog_id"), pk=self.kwargs.get("pk"))


# Add Post
class PostAdd(CreateView):
    model = Post
    template_name = 'blogs_app/post_add.html'
    fields = ['title', 'content']

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, request, *args, **kwargs):
        return super(PostAdd, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        # Form data saving
        new_post_obj = form.save(commit=False)
        new_post_obj.blog = self.request.user
        new_post_obj.save()
        # Adding new post into RSSPost for reflecting a new post on subscriber's news feed.
        blog_subscribers = new_post_obj.blog.userprofile.get_user_subscribers()
        # Notify subscribers.
        new_post_obj.create_rss_post_entry_and_notify_users(blog_subscribers, SITE_DOMAIN)
        return super(PostAdd, self).form_valid(form)

    def get_success_url(self):
        return reverse('blog_posts', kwargs={"blog_id": self.request.user.id})


# Update Post
class PostUpdate(UpdateView):
    model = Post
    fields = ['title', 'content']
    template_name = 'blogs_app/post_add_update_form.html'

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, request, *args, **kwargs):
        post_obj = Post.objects.get(pk=kwargs.get("pk"))
        if post_obj.blog != request.user:
            return HttpResponseForbidden("Not Allowed")
        return super(PostUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form, *args, **kwargs):
        new_post_obj = form.save(commit=False)
        new_post_obj.blog = self.request.user
        new_post_obj.save()
        return super(PostUpdate, self).form_valid(form)

    def get_success_url(self):
        return reverse('blog_posts', kwargs={"blog_id": self.request.user.id})


# Subscribe to Blog
class BlogSubscribe(View):
    # Fill RSSPost table when user subscribes on Blog
    @staticmethod
    def fill_rss_table(user_wants_to_subscribe, user_subscribe_to):
        for post in Post.objects.filter(blog=user_subscribe_to):
            RSSPost.objects.get_or_create(
                user=user_wants_to_subscribe,
                post=post,
                read=False
            )

    @method_decorator(login_required(login_url='login'))
    def get(self, request, blog_id):
        user_wants_to_subscribe = request.user
        user_subscribe_to = User.objects.get(id=blog_id)

        if user_wants_to_subscribe == user_subscribe_to:
            messages.warning(request, 'You can not subscribe on your own Blog.')
        else:
            # Check is there blog in subscriptions table already.
            try:
                SubscriptionsTable.objects.get(
                    subscriber=user_wants_to_subscribe,
                    subscribed_to=user_subscribe_to
                )
                messages.warning(request, 'You already subscribed to this blog.')
            except SubscriptionsTable.DoesNotExist:
                SubscriptionsTable.objects.create(
                    subscriber=user_wants_to_subscribe,
                    subscribed_to=user_subscribe_to
                )

                self.fill_rss_table(user_wants_to_subscribe, user_subscribe_to)
                messages.success(request, 'Subscribed successfully!')
        return redirect('blog_posts', blog_id=blog_id)


# Unsubscribe from Blog
class BlogUnsubscribe(View):
    # Clean RSSPost table when user unsubscribes from Blog
    @staticmethod
    def clean_rss_table(user_wants_to_unsubscribe, user_unsubscribe_from):
        for post in RSSPost.objects.filter(user=user_wants_to_unsubscribe, post__blog=user_unsubscribe_from):
            post.delete()

    @method_decorator(login_required(login_url='login'))
    def get(self, request, blog_id):
        user_wants_to_unsubscribe = request.user
        user_unsubscribe_from = User.objects.get(id=blog_id)
        if user_wants_to_unsubscribe == user_unsubscribe_from:
            messages.warning(request, 'You can not unsubscribe from your own Blog.')
        else:
            SubscriptionsTable.objects.get(
                subscriber=user_wants_to_unsubscribe,
                subscribed_to=user_unsubscribe_from
            ).delete()

            self.clean_rss_table(user_wants_to_unsubscribe, user_unsubscribe_from)
            messages.success(request, 'Unsubscribed successfully!')
        return redirect('blog_posts', blog_id=blog_id)


# News/RSS Feed
@method_decorator(login_required(login_url='login'), name='dispatch')
class NewsFeed(ListView):
    model = RSSPost
    paginate_by = 5
    template_name = 'blogs_app/news_feed.html'

    def get_queryset(self):
        return RSSPost.objects.filter(user=self.request.user).order_by('-post_id')


class MarkPostAsRead(View):
    @method_decorator(login_required(login_url='login'))
    def get(self, *args, **kwargs):
        # If auth user has permission on requested '/feed/mark-read/rss_post_id'
        rss_post_obj = RSSPost.objects.get(pk=kwargs.get('rss_post_id'))
        if self.request.user == rss_post_obj.user:
            rss_post_obj.read = 1
            rss_post_obj.save()
            return JsonResponse({"result": "success"})
        else:
            return HttpResponseForbidden("Not Allowed")
